%define debug_package %{nil}

Name:           uboot-rpi4-config
Version:        2025.01
Release:        3.3%{?dist}
Epoch:          1
Summary:        Package that configures u-boot on a Raspberry Pi 4
License:        GPLv2+ BSD LGPL-2.1+ LGPL-2.0+

ExclusiveArch:  aarch64

Source0:        config.txt
	
BuildRequires:  efi-filesystem
BuildRequires:  efi-srpm-macros
BuildRequires:  uboot-images-armv8

Requires:       bcm2711-firmware

Conflicts:      edk2-rpi4
Conflicts:      boot-rpi4-config

%description
Simple package that installs the right files in the right places to allow a Raspberry Pi 4 to boot with u-boot

%install
# Basically steal the u-boot binaries from uboot-images-armv8 packages
mkdir -p %{buildroot}%{efi_esp_root}
install -m 0700 %{_datadir}/uboot/rpi_arm64/u-boot.bin %{buildroot}%{efi_esp_root}/u-boot.bin
install -p %{SOURCE0} %{buildroot}%{efi_esp_root}

%files
%config(noreplace) %{efi_esp_root}/config.txt
%{efi_esp_root}/u-boot.bin

%changelog
* Tue Feb 25 2025 Luca Magrone <luca@magrone.cc> - 1:2025.01-3.3
- Rebuild for 2025.01-3.3

* Thu May 16 2024 Luca Magrone <luca@magrone.cc> - 1:2024.04-1.1
- Rebuild for 2024.04-1.1

* Fri Jan 12 2024 Luca Magrone <luca@magrone.cc> - 1:2024.01-1.2
- Rebuild for 2024.01-1.2

* Fri Nov 10 2023 Luca Magrone <luca@magrone.cc> - 1:2023.10-0.9.1
- Rebuild for 2023.10-0.9.1
- Add comment
- Drop armv7
- Update config.txt
- Add conflict

* Sun Oct 15 2023 Luca Magrone <luca@magrone.cc> - 2023.10-1
- Rebuild for 2023.10 GA

* Sun Oct 08 2023 Luca Magrone <luca@magrone.cc> - 2023.10-0.4.rc3
- Require bcm2711-firmware instead of the whole bcm283x-firmware
- Rebuild for uboot 2023.10

* Mon Jan 23 2023 Luca Magrone <luca@magrone.cc> - 2023.01-1
- Go back to using the same versioning as uboot
- Rebuild for uboot 2023.01

* Wed Nov 02 2022 Luca Magrone <luca@magrone.cc> - 0-4
- Use the arm64 variant of U-Boot

* Sat Oct 29 2022 Luca Magrone <luca@magrone.cc> - 0-3
- Add %config macro
- Rebuild for patched version of u-boot

* Fri Oct 28 2022 Luca Magrone <luca@magrone.cc> - 0-2
- Transform the package in a simple package that installs configurations since there is already a package for u-boot in Fedora

* Tue Oct 25 2022 Luca Magrone <luca@magrone.cc> - 2022.10-1
- Initial package release
